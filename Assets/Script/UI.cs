using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements; // in order to interact with UIToolkit Component
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{

    private void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;

        Button buttonStart = root.Q<Button>("ButtonStart");
        Button buttonSetting = root.Q<Button>("ButtonSeeting");
        Button buttonExit = root.Q<Button>("ButtonExit");

        // click event handler for the Start button
        buttonStart.clicked += () =>
        {
            // Load the game
            SceneManager.LoadScene("Level 1"); 
        };

        // Setting TBD
        //buttonSetting.clicked += () =>
        //{
        //    Debug.Log("Settings button clicked"); // Placeholder action
        //};

        // click event handler for the Exit
        buttonExit.clicked += () =>
        {
            // Quit the application
#if UNITY_EDITOR
            // 1. Exiting play mode in Unity Editor
            UnityEditor.EditorApplication.isPlaying = false;
#else
            // 2. Quitting the application in a built version
            Application.Quit();
#endif
        };
    }

}
