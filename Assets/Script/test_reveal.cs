using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_reveal : MonoBehaviour
{
    public GameObject[] textSprites; // store current texts on stele (stone)
    private int currentIndex = -1; // current text index, -1 stands for the default stage, which is "not lighting up"
    void Start()
    {
        // Set all "text" to be "Hidden" by default
        foreach (GameObject textSprite in textSprites)
        {
            textSprite.SetActive(false);
        }
    }

    void Update()
    {
        // Condition statement of lighting text: mouse is clicked AND player is next to the stele
        if (Input.GetMouseButtonDown(0) && IsPlayerNearby())
        {
            RevealNextText();
        }
    }

    void RevealNextText()
    {
        // Condition statement for: if one character is lighting up, it will turn hidden before the next character lightting up
        if (currentIndex >= 0 && currentIndex < textSprites.Length)
        {
            textSprites[currentIndex].SetActive(false);
        }

        // Update the text_index to the next index
        currentIndex++;

        // Check if all texts are lighten up once
        if (currentIndex < textSprites.Length)
        {
            // Light the next character/text
            textSprites[currentIndex].SetActive(true);
        }
        else
        {
            // if it's the 5th click, light all characters up (*all characters one one stele*)
            foreach (GameObject text in textSprites)
            {
                text.SetActive(true);
            }
            // Reset index for next stele
            currentIndex = -1;
            foreach (GameObject textSprite in textSprites)
            {
                textSprite.SetActive(false);
            }
        }
    }

    private bool isPlayerNearby = false; // If player is nearby the stele

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) // * Here you have to have a "Player" tag on player
        {
            isPlayerNearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player")) // * Here you have to have a "Player" tag on player
        {
            isPlayerNearby = false;
        }
    }

    bool IsPlayerNearby()
    {
        return isPlayerNearby;
    }
}

