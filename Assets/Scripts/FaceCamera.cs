using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    // Create an "Array" that get all child objects in Environments collections
    Transform[] environment_childs;

    void Start()
    {
        environment_childs = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            environment_childs[i] = transform.GetChild(i);
        }
    }

    void Update()
    {
        for (int i = 0; i < environment_childs.Length; i++)
        {
            environment_childs[i].rotation = Camera.main.transform.rotation;
        }
    }
}