using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Player : MonoBehaviour
{
    [SerializeField] public float move_speed = 5f;
    private Rigidbody2D rb;
    private Animator animator;
    private float Speed; // "Speed" set in Animator that determine if idle (Speed = 0) or run (Speed = 1)

    private Vector3 offset;                                                 // CAMERA FACING Scripts

    void Start()
    {
        offset = Camera.main.transform.position - transform.position;       // CAMERA FACING Scripts
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float InputX = Input.GetAxis("Horizontal"); // get horizontal move input
        transform.position += new Vector3(InputX, 0, 0) * move_speed * Time.deltaTime;
        float InputY = Input.GetAxis("Vertical"); // get vertical move input
        Vector2 input = new Vector2(InputX, InputY).normalized; // Fix the probelm of speed, when hitting both vertical and horizontal key, palyer speed will doubled
        transform.position += new Vector3(0, InputY, 0) * move_speed * Time.deltaTime;

        // Below are the codes for flipping sprite based on horizontal direction

        // If character move left, flip sprite
        if (InputX < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        // If move right (default one), recover to original
        else if (InputX > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        //// Update Speed parameter based on moveInput
        //Speed = Mathf.Abs(InputX); // This ensures Speed is 0 when idle and increases with movement
        //Speed = Mathf.Abs(InputY); // Do the same thing for Y-Axis 
        //animator.SetFloat("Speed", Speed); // Pass Speed to Animator to switch between idle and run
        
        // Update Speed parameter based on overall movement
        Speed = input.magnitude; // This takes both X and Y input into account
        animator.SetFloat("Speed", Speed); // Pass Speed to Animator to switch between idle and run

        Camera.main.transform.position = transform.position + offset;       // CAMERA FACING Scripts
    }
}
